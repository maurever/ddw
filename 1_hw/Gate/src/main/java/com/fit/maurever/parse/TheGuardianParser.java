/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fit.maurever.parse;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Veronika Maurerova <veronika at maurerova.cz>
 */
public class TheGuardianParser {

    private final ArrayList<Article> articles;

    private final String mainPage = "http://www.bbc.co.uk";
    private final String mainArchiveUrlStart = "http://www.theguardian.com/world/europe-news/";
    private final String mainArchiveUrlEnd = "/all";
    private final SimpleDateFormat ft = new SimpleDateFormat("yyyy/MMM/dd", Locale.ENGLISH);
    private final SimpleDateFormat ft2 = new SimpleDateFormat("yyyy-MM-dd");

    public TheGuardianParser(Date date) {
        articles = new ArrayList<>();
        downloadArticles(date);
    }

    public ArrayList<Article> getArticles() {
        return this.articles;
    }

    private void downloadArticles(Date date) {
        String dateString = ft.format(date).toLowerCase();
        Document mainPageDoc = null;
        while (mainPageDoc == null) {
            try {
                mainPageDoc = Jsoup.connect(mainArchiveUrlStart + dateString+ mainArchiveUrlEnd).get();
                Elements elements = mainPageDoc.select("a[class=\"u-faux-block-link__overlay js-headline-text\"]");
                for (Element element : elements) {
                    articles.add(readArticle(element.attr("href"), date, element.text()));
                }
            } catch (IOException ex) {
                ex.printStackTrace();
                System.out.println("disconnect");
            }
        }
    }

    private Article readArticle(String url, Date date, String title) {
        Document articlePageDoc = null;
        Article article = null;
        while (articlePageDoc == null) {
            try {
                articlePageDoc = Jsoup.connect(url).get();
                String content = "";
                ArrayList<String> tagList = new ArrayList<>();
                Elements contentElements = articlePageDoc.select("div[class=\"content__article-body from-content-api js-article__body\"] p");
                for (Element contentElement : contentElements) {
                    
                    content += "\n" + contentElement.text();
                }
                Elements tagElements = articlePageDoc.select("a[class=\"  button button--small button--tag button--secondary\"]");
                for (Element tagElement : tagElements) {
                    tagList.add(tagElement.text());
                }
                content = content.length() > 0 ? content.substring(1) : content;
                article = new Article(title, content, tagList, ft2.format(date));
            } catch (IOException ex) {
                System.out.println("disconnect");
            }
        }
        return article;
    }
}
