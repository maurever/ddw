package com.fit.maurever.gate;

import com.fit.maurever.lexicon.Lexicon;
import com.fit.maurever.parse.Article;
import com.fit.maurever.statistic.ArticleStatistic;
import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.CreoleRegister;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.Node;
import gate.ProcessingResource;
import gate.creole.SerialAnalyserController;
import gate.util.GateException;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Milan Dojchinovski
 * <milan (at) dojchinovski (dot) mk>
 * Twitter: @m1ci www: http://dojchinovski.mk
 */
public class GateClient {

    // corpus pipeline
    private static SerialAnalyserController annotationPipeline = null;

    // whether the GATE is initialised
    private static boolean isGateInitilised = false;

    //lexicon
    private static Lexicon lexicon = new Lexicon();

    public ArticleStatistic run(Article article) {
        if (!isGateInitilised) {
            // initialise GATE
            initialiseGate();
        }
        ArticleStatistic statistic = null;
        try {
            // create a document and corpus
            String text = article.getContent();
            if (text.length() == 0) {
                return new ArticleStatistic(article.getDate(), "", 0);
            }
            Document document = Factory.newDocument(text);
            Corpus corpus = Factory.newCorpus("");
            corpus.add(document);

            // set the corpus to the pipeline
            annotationPipeline.setCorpus(corpus);

            //run the pipeline
            annotationPipeline.execute();

            // loop through the documents in the corpus
            for (int i = 0; i < corpus.size(); i++) {
                Document doc = corpus.get(i);

                // get the default annotation set
                AnnotationSet as_default = doc.getAnnotations();

                FeatureMap featureMap = null;

                // get all Token annotations
                AnnotationSet annSetSentences = as_default.get("Sentence", featureMap);

                // get all Token annotations
                AnnotationSet annSetTokens = as_default.get("Token", featureMap);

                statistic = new ArticleStatistic(article.getDate(), article.getStringTags(), annSetSentences.size());

                ArrayList tokenAnnotations = new ArrayList(annSetTokens);
                // looop through the Token annotations
                for (int j = 0; j < tokenAnnotations.size(); ++j) {

                    // get a token annotation
                    Annotation token = (Annotation) tokenAnnotations.get(j);

                    // get the underlying string for the Token
                    Node isaStart = token.getStartNode();
                    Node isaEnd = token.getEndNode();
                    String tokenText = doc.getContent().getContent(isaStart.getOffset(), isaEnd.getOffset()).toString().toLowerCase();
                    // get the features of the token
                    FeatureMap annFM = token.getFeatures();

                    String kind = annFM.get("kind").toString();
                    String category = annFM.get("category").toString();
                    if (kind.equals("word")) {
                        if (category.equals("NN") && tokenText.length() > 1) {
                            statistic.update(tokenText, lexicon.getEmotions(tokenText));
                        } else {
                            statistic.update(tokenText);
                        }
                    }
                }
            }
        } catch (GateException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return statistic;
    }

    private void initialiseGate() {

        try {
            // set GATE home folder
            // Eg. /Applications/GATE_Developer_7.0
            File gateHomeFile = new File("/home/mori/Documents/4_semestr/DDW/GATE");
            Gate.setGateHome(gateHomeFile);

            // set GATE plugins folder
            // Eg. /Applications/GATE_Developer_7.0/plugins            
            File pluginsHome = new File("/home/mori/Documents/4_semestr/DDW/GATE/plugins");
            Gate.setPluginsHome(pluginsHome);

            // set user config file (optional)
            // Eg. /Applications/GATE_Developer_7.0/user.xml
            Gate.setUserConfigFile(new File("/home/mori/Documents/4_semestr/DDW/GATE", "user.xml"));

            // initialise the GATE library
            Gate.init();

            // load ANNIE plugin
            CreoleRegister register = Gate.getCreoleRegister();
            URL annieHome = new File(pluginsHome, "ANNIE").toURL();
            register.registerDirectories(annieHome);

            // flag that GATE was successfuly initialised
            isGateInitilised = true;

            // create an instance of a Document Reset processing resource
            ProcessingResource documentResetPR = (ProcessingResource) Factory.createResource("gate.creole.annotdelete.AnnotationDeletePR");
            // create an instance of a English Tokeniser processing resource
            ProcessingResource tokenizerPR = (ProcessingResource) Factory.createResource("gate.creole.tokeniser.DefaultTokeniser");
            // create an instance of a Sentence Splitter processing resource
            ProcessingResource sentenceSplitterPR = (ProcessingResource) Factory.createResource("gate.creole.splitter.SentenceSplitter");
            ProcessingResource posTagger = (ProcessingResource) Factory.createResource("gate.creole.POSTagger");

            // create corpus pipeline
            annotationPipeline = (SerialAnalyserController) Factory.createResource("gate.creole.SerialAnalyserController");

            // add the processing resources (modules) to the pipeline
            annotationPipeline.add(documentResetPR);
            annotationPipeline.add(tokenizerPR);
            annotationPipeline.add(sentenceSplitterPR);
            annotationPipeline.add(posTagger);

        } catch (MalformedURLException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (GateException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
