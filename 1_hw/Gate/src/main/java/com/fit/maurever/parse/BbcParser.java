/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fit.maurever.parse;

import java.io.IOException;
import java.util.ArrayList;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Veronika Maurerova <veronika at maurerova.cz>
 */
public class BbcParser {

    private final ArrayList<Article> articles;

    private final String mainPage = "http://www.bbc.co.uk";
    private final String mainArchiveUrl = "http://www.bbc.co.uk/informationandarchives/search?date=";

    public BbcParser(String date) {
        articles = new ArrayList<>();
        downloadArticles(date);
    }

    public ArrayList<Article> getArticles() {
        return this.articles;
    }

    private void downloadArticles(String date) {
        Document mainPageDoc = null;
        while (mainPageDoc == null) {
            try {
                mainPageDoc = Jsoup.connect(mainArchiveUrl + date).get();
                Elements elements = mainPageDoc.select("li.item h3 a");
                for (Element element : elements) {
                    articles.add(readArticle(mainPage + element.attr("href"), date, element.text()));
                }
            } catch (IOException ex) {
                System.out.println("disconnect");
            }
        }
    }

    private Article readArticle(String url, String date, String title) {
        Document articlePageDoc = null;
        Article article = null;
        while (articlePageDoc == null) {
            try {
                articlePageDoc = Jsoup.connect(url).get();
                Elements elements = articlePageDoc.select("div[role=\"article\"]");
                String content = "";
                ArrayList<String> tagList = new ArrayList<>();
                if (elements.size() > 0) {
                    Elements contentElements = elements.select("div.text p");
                    for (Element contentElement : contentElements) {
                        content += "\n" + contentElement.text();
                    }
                    Elements tagElements = elements.select("div.tags a");
                    for (Element tagElement : tagElements) {
                        tagList.add(tagElement.text());
                    }
                    content = content.length() > 0 ? content.substring(1) : content;
                }
                article = new Article(title, content, tagList, date);
            } catch (IOException ex) {
                System.out.println("disconnect");
            }
        }
        return article;
    }
}
