/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fit.maurever.lexicon;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Veronika Maurerova <veronika at maurerova.cz>
 */
public class Lexicon {

    private final String driver = "org.apache.derby.jdbc.EmbeddedDriver";
    private final String database = "jdbc:derby://localhost:1527/emotion_lexicon";
    
    private Connection con;

    public void loadDataFromFileToDatabase() {
        BufferedReader reader = null;
        int id = 0;
        try {
            Class.forName(driver).newInstance();
            con = DriverManager.getConnection(database);
            Statement sta = con.createStatement();
            String line;

            reader = new BufferedReader(new FileReader("files/lexicon.txt"));
            while ((line = reader.readLine()) != null) {
                System.out.println(id);
                String[] words = line.split("\t");
                sta.executeUpdate("INSERT INTO LEXICON"
                        + " (ID, WORD, EMOTION, ASSOCIATED)"
                        + " VALUES (" + (id++) + ",'" + words[0] + "','" + words[1] + "'," + (words[2].equals("1") ? "true" : "false") + ")");
            }
            sta.close();
            con.close();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException | IOException e) {
            System.err.println("Exception: " + e.getMessage());
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException ex) {
            }
        }
    }

    public List<String> getEmotions(String word) {
        List<String> emotions = new ArrayList<>();
        try {
            Class.forName(driver).newInstance();
            con = DriverManager.getConnection(database);
            Statement sta = con.createStatement();
            ResultSet res = sta.executeQuery("SELECT EMOTION FROM LEXICON WHERE WORD LIKE'"+ word + "' AND ASSOCIATED = TRUE");
            while (res.next()) {
                emotions.add(res.getString("EMOTION"));
            }
            res.close();
            sta.close();
            con.close();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
            System.err.println("Exception: " + e.getMessage());
        }
        return emotions;
    }

    public static void main(String[] args) {
       new Lexicon().loadDataFromFileToDatabase();
    }
}
