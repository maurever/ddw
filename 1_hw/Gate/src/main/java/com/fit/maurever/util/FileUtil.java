/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fit.maurever.util;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

/**
 *
 * @author Veronika Maurerova <veronika at maurerova.cz>
 */
public class FileUtil {

    public static void writeFile(String text, String fileUrl) {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(fileUrl, "UTF-8");
            writer.print(text);
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            System.out.println("File " + fileUrl + " was not created.");
        } finally {
            if (writer != null) {
                writer.close();
                System.out.println("File " + fileUrl + " was created.");
            }
        }
    }
}
