/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fit.maurever.statistic;

import com.fit.maurever.util.MapUtil;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author Veronika Maurerova <veronika at maurerova.cz>
 */
public class ArticleStatistic {

    private final String date;
    private final int sentenceNumber;
    private int wordNumber;
    private int positiveNumber;
    private int negativeNumber;
    private int angerNumber;
    private int fearNumber;
    private int anticipationNumber;
    private int trustNumber;
    private int surpriseNumber;
    private int sadnessNumber;
    private int joyNumber;
    private int disgustNumber;
    private int noEmotionNumber;
    private final String tags;
    private final Map<String, Integer> words;

    public ArticleStatistic(String date, String tags, int sentenceNumber) {
        this.date = date;
        this.wordNumber = 0;
        this.sentenceNumber = sentenceNumber;
        this.positiveNumber = 0;
        this.negativeNumber = 0;
        this.angerNumber = 0;
        this.fearNumber = 0;
        this.anticipationNumber = 0;
        this.trustNumber = 0;
        this.surpriseNumber = 0;
        this.sadnessNumber = 0;
        this.joyNumber = 0;
        this.disgustNumber = 0;
        this.noEmotionNumber = 0;
        this.tags = tags;
        this.words = new HashMap<>();
    }

    public void update(String word) {
        wordNumber++;
        //updateMap(word);
    }

    public void update(String word, List<String> emotions) {
        wordNumber++;
        updateEmotions(emotions);
        updateMap(word);
    }

    private void updateEmotions(List<String> emotions) {
        if (emotions.size() > 0) {
            for (String emotion : emotions) {
                switch (emotion) {
                    case "positive":
                        positiveNumber++;
                        break;
                    case "negative":
                        negativeNumber++;
                        break;
                    case "anger":
                        angerNumber++;
                        break;
                    case "fear":
                        fearNumber++;
                        break;
                    case "anticipation":
                        anticipationNumber++;
                        break;
                    case "trust":
                        trustNumber++;
                        break;
                    case "surprise":
                        surpriseNumber++;
                        break;
                    case "sadness":
                        sadnessNumber++;
                        break;
                    case "joy":
                        joyNumber++;
                        break;
                    case "disgust":
                        disgustNumber++;
                }
            }
        } else {
            noEmotionNumber++;
        }
    }

    private void updateMap(String word) {
        if (words.containsKey(word)) {
            int number = words.remove(word);
            words.put(word, ++number);
        } else {
            words.put(word, 1);
        }
    }

    private String tenFirstWords() {
        String wordsString = "";
        Map<String, Integer> sortedMap = MapUtil.sortByValue(words);
        int i = 0;
        for (Entry<String, Integer> entrySet : sortedMap.entrySet()) {
            if (i < 10) {
                wordsString += "," + entrySet.getKey();
                i++;
            }
        }
        return wordsString;
    }

    public String getHeader() {
        return "date, wordNumber, sentenceNumber, positiveNumber, negativeNumber, angerNumber, fearNumber, anticipationNumber, "
                + "trustNumber, surpriseNumber, sadnessNumber, joyNumber, disgustNumber, noEmotionNumber, tags, "
                + "words1, words2, words3, words4, words5, words6, words7, words8, words9, words10";
    }

    @Override
    public String toString() {
        return date + "," + wordNumber + "," + sentenceNumber + "," + positiveNumber + "," + negativeNumber + "," + angerNumber
                + "," + fearNumber + "," + anticipationNumber + "," + trustNumber + "," + surpriseNumber
                + "," + sadnessNumber + "," + joyNumber + "," + disgustNumber + "," + noEmotionNumber
                + ",\"" + tags + "\"" + tenFirstWords();
    }

}
