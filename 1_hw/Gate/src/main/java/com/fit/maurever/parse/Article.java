package com.fit.maurever.parse;

import java.util.ArrayList;

/**
 *
 * @author Veronika Maurerova <veronika at maurerova.cz>
 */
public class Article {

    private String title;
    private String content;
    private ArrayList<String> tags;
    private String date;

    public Article(String title, String content, ArrayList<String> tags, String date) {
        this.title = title;
        this.content = content;
        this.tags = tags;
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStringTags() {
        String tagString = "";
        for (String tag : tags) {
            tagString += " " + tag;
        }
        return tagString.length() > 0 ? tagString.substring(1) : tagString;
    }

    @Override
    public String toString() {
        return "Article{\n" + "title: " + title + "\n\ncontent:\n" + content + "\n\ntags:" + getStringTags() + "\ndate:" + date + "\n}";
    }

}
