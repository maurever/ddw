/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fit.maurever.run;

import com.fit.maurever.gate.GateClient;
import com.fit.maurever.parse.Article;
import com.fit.maurever.parse.BbcParser;
import com.fit.maurever.parse.TheGuardianParser;
import com.fit.maurever.util.FileUtil;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Veronika Maurerova <veronika at maurerova.cz>
 */
public class Main {

    public static void main(String[] args) {
        GateClient client = new GateClient();
        StringBuilder result = new StringBuilder();
        try {
            SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
            Date startDate = ft.parse("2014-05-15");
            Date endDate = ft.parse("2015-03-31");
            Calendar start = Calendar.getInstance();
            start.setTime(startDate);
            Calendar end = Calendar.getInstance();
            end.setTime(endDate);
            for (Date date = start.getTime(); start.before(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
                TheGuardianParser parser = new TheGuardianParser(date);
                List<Article> articles = parser.getArticles();
                for (Article article : articles) {
                    String tmpResult = client.run(article).toString();
                    System.out.println(tmpResult);
                    result.append(tmpResult).append("\n");
                }
            }
            FileUtil.writeFile(result.toString(), "files/result.txt");
        } catch (java.text.ParseException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
